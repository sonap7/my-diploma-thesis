// sampletest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include <opencv\cv.h>
#include <opencv\highgui.h>
#include <opencv2\video\background_segm.hpp>
#include <vector>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp> 

#include <NuiApi.h>
#include <NuiImageCamera.h>
#include <NuiSensor.h>

#include <stdio.h>
#include <sstream>
#include <fstream>

using namespace cv;
using namespace std;

CvPoint SkeletonToScreen(Vector4 skeletonPoint, int width, int height, const
NUI_IMAGE_FRAME *imgFrame)
{
LONG x, y;
USHORT depth;
LONG ColorX, ColorY;
NuiTransformSkeletonToDepthImage(skeletonPoint, &x, &y, &depth);
NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(NUI_IMAGE_RESOLUTION_640x480,
                                                NUI_IMAGE_RESOLUTION_320x240,
                                                &imgFrame->ViewArea,
                                                x,
                                                y,
                                                depth,
                                                &ColorX,
                                                &ColorY);
float screenPointX = static_cast<float>(ColorX * width) / 640;
float screenPointY = static_cast<float>(ColorY * height) / 480; 
return cvPoint(screenPointX, screenPointY);
} 

int main()
{

IplImage *frame = cvCreateImage(cvSize(640, 480), 8, 3);

float lfx, lfy, lfz, rfx, rfy, rfz, lax, lay, laz, rax, ray, raz, lkx, lky, lkz, rkx, rky, rkz ;
int choise ;

ofstream ofs;              //open *txt file
ofs.open ("example.txt");

HRESULT hr;
hr = NuiInitialize(NUI_INITIALIZE_FLAG_USES_SKELETON | NUI_INITIALIZE_FLAG_USES_COLOR);  //Get Skeleton

if(hr != S_OK)
 return hr; 

HANDLE SkeletonEvent = CreateEventW(NULL, TRUE, FALSE, NULL);
hr = NuiSkeletonTrackingEnable(SkeletonEvent, 0);
if(hr != S_OK)
    return hr;

HANDLE ColorEvent = CreateEventW(NULL, TRUE, FALSE, NULL);
HANDLE h2 = NULL;

hr = NuiImageStreamOpen(NUI_IMAGE_TYPE_COLOR, NUI_IMAGE_RESOLUTION_640x480, 0, 2, ColorEvent, &h2);
if(hr != S_OK)
    return hr;

NUI_SKELETON_FRAME skeletonFrame = {0};  //Declare our frame

for (;;) {

	cout << "What joint do you want to track ?" << endl << 
			"1. ANKLE" << endl << 
			"2. KNEE" << endl << 
			"3. FOOT" << endl <<
			"4. ANKLE and KNEE" << endl << 
			"5. ANKLE and FOOT" << endl << 
			"6. KNEE and FOOT" << endl << 
			"7. ANKLE and KNEE and FOOT" << endl ;   //Choose

	cin >> choise ;

	if ( choise == 1) 
	{
		ofs << " Recorded Ankle " << endl << " Sample Frequency (Hz) " << endl << " 100 Hz / 0,1 sec " << endl << endl ;

while (1) //For all of time  
{

    WaitForSingleObject(SkeletonEvent, INFINITE);
    
	//get skeleton data
    hr = NuiSkeletonGetNextFrame(0, &skeletonFrame);   //Get a frame and stuff it into ourframe
    if( hr != S_OK )
        continue;

    NuiTransformSmooth(&skeletonFrame, NULL);

    //get color image data
    const NUI_IMAGE_FRAME * pImageFrame = NULL;
    
	hr = NuiImageStreamGetNextFrame(h2, 0, &pImageFrame );
	
	if( hr != S_OK )
        continue;

    INuiFrameTexture *pTexture = pImageFrame->pFrameTexture;
    NUI_LOCKED_RECT LockedRect;
    pTexture->LockRect( 0, &LockedRect, NULL, 0 );
       
	//Draw skeleton points on frame 
    for(int i=0; i<6; i++){		// up to 6 person
        NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;
        if (NUI_SKELETON_TRACKED == trackingState)
            
                cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				ray = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].y ;
				lay = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].y ;
				rax = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].x ;
				lax = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].x ;		  
				raz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].z ; 
				laz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].z ; 

							cout << "Right Ankle.y: " ;
							cout << ray << endl ;
							cout << "Left Ankle.y: " ;
							cout << lay << endl ;
							cout << "Right Ankle.x: " ;
							cout << rax << endl ;
							cout << "Left Ankle.x: " ;
							cout << lax << endl ;
							cout << "Right Ankle.z: " ;
							cout << raz << endl ;
							cout << "Left Ankle.z: " ;
							cout << laz << endl ; 

							ofs << "Right Ankle.y: "  << ray << endl ;
							ofs << "Left Ankle.y: "  << lay << endl ;
							ofs << "Right Ankle.x: "  << rax << endl ;
							ofs << "Left Ankle.x: "  << lax << endl ;
							ofs << "Right Ankle.z: "  << raz << endl ;
							ofs << "Left Ankle.y: "  << laz << endl ;
							ofs << endl ;
    }

	 cvShowImage("Frame",frame);
    NuiImageStreamReleaseFrame( h2, pImageFrame );
    cvWaitKey(30); 
	
	Sleep(100);
	
	}
}

	else if ( choise == 2) 
	{

		ofs << " Recorded Knee " << endl << " Sample Frequency (Hz) " << endl << " 100 Hz / 0,1 sec " << endl << endl ;

while(1)
{

    WaitForSingleObject(SkeletonEvent, INFINITE);
    //get skeleton data
    hr = NuiSkeletonGetNextFrame(0, &skeletonFrame);
    if( hr != S_OK )
        continue;
    NuiTransformSmooth(&skeletonFrame, NULL);
    //get color image data
    const NUI_IMAGE_FRAME * pImageFrame = NULL;
    hr = NuiImageStreamGetNextFrame(h2, 0, &pImageFrame );
    if( hr != S_OK )
        continue;

    INuiFrameTexture *pTexture = pImageFrame->pFrameTexture;
    NUI_LOCKED_RECT LockedRect;
    pTexture->LockRect( 0, &LockedRect, NULL, 0 );
   
	//Draw skeleton points on frame 
    for(int i=0; i<6; i++){
        NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;
        if (NUI_SKELETON_TRACKED == trackingState) 
          
                cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				rky = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].y ;
				lky = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].y ;
				rkx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].x ;
				lkx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].x ;		  
				rkz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].z ; 
				lkz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].z ; 

							cout << "Right Knee.y: " ;
							cout << rky << endl ;
							cout << "Left Knee.y: " ;
							cout << lky << endl ;
							cout << "Right Knee.x: " ;
							cout << rkx << endl ;
							cout << "Left Knee.x: " ;
							cout << lkx << endl ;
							cout << "Right Knee.z: " ;
							cout << rkz << endl ;
							cout << "Left Knee.z: " ;
							cout << lkz << endl ; 

							ofs << "Right Knee.y: "  << rky << endl ;
							ofs << "Left Knee.y: "  << lky << endl ;
							ofs << "Right Knee.x: "  << rkx << endl ;
							ofs << "Left Knee.x: "  << lkx << endl ;
							ofs << "Right Knee.z: "  << rkz << endl ;
							ofs << "Left Knee.y: "  << lkz << endl ;
							ofs << endl ;
		
    }
    cvShowImage("Frame",frame);
    NuiImageStreamReleaseFrame( h2, pImageFrame );
    cvWaitKey(30);

	Sleep(100);

	}
}

	else if ( choise == 3) 
	{

		ofs << " Recorded Foot " << endl << " Sample Frequency (Hz) " << endl << " 100 Hz / 0,1 sec " << endl << endl ;

while(1)
{

    WaitForSingleObject(SkeletonEvent, INFINITE);
    //get skeleton data
    hr = NuiSkeletonGetNextFrame(0, &skeletonFrame);
    if( hr != S_OK )
        continue;
    NuiTransformSmooth(&skeletonFrame, NULL);
    //get color image data
    const NUI_IMAGE_FRAME * pImageFrame = NULL;
    hr = NuiImageStreamGetNextFrame(h2, 0, &pImageFrame );
    if( hr != S_OK )
        continue;

    INuiFrameTexture *pTexture = pImageFrame->pFrameTexture;
    NUI_LOCKED_RECT LockedRect;
    pTexture->LockRect( 0, &LockedRect, NULL, 0 );
   
	//Draw skeleton points on frame 
    for(int i=0; i<6; i++){
        NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;
        if (NUI_SKELETON_TRACKED == trackingState)
            
                cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				rfy = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].y ;
				lfy = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].y ;
				rfx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].x ;
				lfx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].x ;		  
				rfz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].z ; 
				lfz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].z ; 

				            cout << "Right Foot.y: " ;
							cout << rfy << endl ;
							cout << "Left Foot.y: " ;
							cout << lfy << endl ;
							cout << "Right Foot.x: " ;
							cout << rfx << endl ;
							cout << "Left Foot.x: " ;
							cout << lfx << endl ;
							cout << "Right Foot.z: " ;
							cout << rfz << endl ;
							cout << "Left Foot.z: " ;
							cout << lfz << endl ; 

							ofs << "Right Foot.y: "  << rfy << endl ;
							ofs << "Left Foot.y: "  << lfy << endl ;
							ofs << "Right Foot.x: "  << rfx << endl ;
							ofs << "Left Foot.x: "  << lfx << endl ;
							ofs << "Right Foot.z: "  << rfz << endl ;
							ofs << "Left Foot.y: "  << lfz << endl ;
							ofs << endl ;
    }
    cvShowImage("Frame",frame);
    NuiImageStreamReleaseFrame( h2, pImageFrame );
    cvWaitKey(30);

	Sleep(100);

	}
}

else if ( choise == 4) 
	{

		ofs << " Recorded Ankle and Knee " << endl << " Sample Frequency (Hz) " << endl << " 100 Hz / 0,1 sec " << endl << endl ;

while(1)
{

    WaitForSingleObject(SkeletonEvent, INFINITE);
    //get skeleton data
    hr = NuiSkeletonGetNextFrame(0, &skeletonFrame);
    if( hr != S_OK )
        continue;
    NuiTransformSmooth(&skeletonFrame, NULL);
    //get color image data
    const NUI_IMAGE_FRAME * pImageFrame = NULL;
    hr = NuiImageStreamGetNextFrame(h2, 0, &pImageFrame );
    if( hr != S_OK )
        continue;

    INuiFrameTexture *pTexture = pImageFrame->pFrameTexture;
    NUI_LOCKED_RECT LockedRect;
    pTexture->LockRect( 0, &LockedRect, NULL, 0 );
   
	//Draw skeleton points on frame 
    for(int i=0; i<6; i++){
        NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;
        if (NUI_SKELETON_TRACKED == trackingState) 
          
                cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				rky = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].y ;
				lky = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].y ;
				rkx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].x ;
				lkx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].x ;		  
				rkz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].z ; 
				lkz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].z ; 

				ray = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].y ;
				lay = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].y ;
				rax = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].x ;
				lax = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].x ;		  
				raz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].z ; 
				laz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].z ; 

							cout << "Right Knee.y: " ;
							cout << rky << endl ;
							cout << "Left Knee.y: " ;
							cout << lky << endl ;
							cout << "Right Knee.x: " ;
							cout << rkx << endl ;
							cout << "Left Knee.x: " ;
							cout << lkx << endl ;
							cout << "Right Knee.z: " ;
							cout << rkz << endl ;
							cout << "Left Knee.z: " ;
							cout << lkz << endl ; 

							cout << "Right Ankle.y: " ;
							cout << ray << endl ;
							cout << "Left Ankle.y: " ;
							cout << lay << endl ;
							cout << "Right Ankle.x: " ;
							cout << rax << endl ;
							cout << "Left Ankle.x: " ;
							cout << lax << endl ;
							cout << "Right Ankle.z: " ;
							cout << raz << endl ;
							cout << "Left Ankle.z: " ;
							cout << laz << endl ; 

							ofs << "Right Knee.y: "  << rky << endl ;
							ofs << "Left Knee.y: "  << lky << endl ;
							ofs << "Right Knee.x: "  << rkx << endl ;
							ofs << "Left Knee.x: "  << lkx << endl ;
							ofs << "Right Knee.z: "  << rkz << endl ;
							ofs << "Left Knee.y: "  << lkz << endl ;
							ofs << endl ;

							ofs << "Right Ankle.y: "  << ray << endl ;
							ofs << "Left Ankle.y: "  << lay << endl ;
							ofs << "Right Ankle.x: "  << rax << endl ;
							ofs << "Left Ankle.x: "  << lax << endl ;
							ofs << "Right Ankle.z: "  << raz << endl ;
							ofs << "Left Ankle.y: "  << laz << endl ;
							ofs << endl ;
		
    }
    cvShowImage("Frame",frame);
    NuiImageStreamReleaseFrame( h2, pImageFrame );
    cvWaitKey(30);

	Sleep(100);

	}
}

else if ( choise == 5) 
	{

		ofs << " Recorded Ankle and Foot " << endl << " Sample Frequency (Hz) " << endl << " 100 Hz / 0,1 sec " << endl << endl ;

while(1)
{

    WaitForSingleObject(SkeletonEvent, INFINITE);
    //get skeleton data
    hr = NuiSkeletonGetNextFrame(0, &skeletonFrame);
    if( hr != S_OK )
        continue;
    NuiTransformSmooth(&skeletonFrame, NULL);
    //get color image data
    const NUI_IMAGE_FRAME * pImageFrame = NULL;
    hr = NuiImageStreamGetNextFrame(h2, 0, &pImageFrame );
    if( hr != S_OK )
        continue;

    INuiFrameTexture *pTexture = pImageFrame->pFrameTexture;
    NUI_LOCKED_RECT LockedRect;
    pTexture->LockRect( 0, &LockedRect, NULL, 0 );
   
	//Draw skeleton points on frame 
    for(int i=0; i<6; i++){
        NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;
        if (NUI_SKELETON_TRACKED == trackingState) 
          
                cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				rfy = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].y ;
				lfy = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].y ;
				rfx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].x ;
				lfx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].x ;		  
				rfz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].z ; 
				lfz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].z ; 

				ray = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].y ;
				lay = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].y ;
				rax = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].x ;
				lax = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].x ;		  
				raz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].z ; 
				laz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].z ;

							cout << "Right Foot.y: " ;
							cout << rfy << endl ;
							cout << "Left Foot.y: " ;
							cout << lfy << endl ;
							cout << "Right Foot.x: " ;
							cout << rfx << endl ;
							cout << "Left Foot.x: " ;
							cout << lfx << endl ;
							cout << "Right Foot.z: " ;
							cout << rfz << endl ;
							cout << "Left Foot.z: " ;
							cout << lfz << endl ; 

							cout << "Right Ankle.y: " ;
							cout << ray << endl ;
							cout << "Left Ankle.y: " ;
							cout << lay << endl ;
							cout << "Right Ankle.x: " ;
							cout << rax << endl ;
							cout << "Left Ankle.x: " ;
							cout << lax << endl ;
							cout << "Right Ankle.z: " ;
							cout << raz << endl ;
							cout << "Left Ankle.z: " ;
							cout << laz << endl ; 

							ofs << "Right Foot.y: "  << rfy << endl ;
							ofs << "Left Foot.y: "  << lfy << endl ;
							ofs << "Right Foot.x: "  << rfx << endl ;
							ofs << "Left Foot.x: "  << lfx << endl ;
							ofs << "Right Foot.z: "  << rfz << endl ;
							ofs << "Left Foot.y: "  << lfz << endl ;
							ofs << endl ;

							ofs << "Right Ankle.y: "  << ray << endl ;
							ofs << "Left Ankle.y: "  << lay << endl ;
							ofs << "Right Ankle.x: "  << rax << endl ;
							ofs << "Left Ankle.x: "  << lax << endl ;
							ofs << "Right Ankle.z: "  << raz << endl ;
							ofs << "Left Ankle.y: "  << laz << endl ;
							ofs << endl ;
		
    }
    cvShowImage("Frame",frame);
    NuiImageStreamReleaseFrame( h2, pImageFrame );
    cvWaitKey(30);

	Sleep(100);

	}
}

else if ( choise == 6) 
	{

		ofs << " Recorded Knee and Foot " << endl << " Sample Frequency (Hz) " << endl << " 100 Hz / 0,1 sec " << endl << endl ;

while(1)
{

    WaitForSingleObject(SkeletonEvent, INFINITE);
    //get skeleton data
    hr = NuiSkeletonGetNextFrame(0, &skeletonFrame);
    if( hr != S_OK )
        continue;
    NuiTransformSmooth(&skeletonFrame, NULL);
    //get color image data
    const NUI_IMAGE_FRAME * pImageFrame = NULL;
    hr = NuiImageStreamGetNextFrame(h2, 0, &pImageFrame );
    if( hr != S_OK )
        continue;

    INuiFrameTexture *pTexture = pImageFrame->pFrameTexture;
    NUI_LOCKED_RECT LockedRect;
    pTexture->LockRect( 0, &LockedRect, NULL, 0 );
   
	//Draw skeleton points on frame 
    for(int i=0; i<6; i++){
        NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;
        if (NUI_SKELETON_TRACKED == trackingState) 
          
                cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				rfy = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].y ;
				lfy = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].y ;
				rfx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].x ;
				lfx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].x ;		  
				rfz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].z ; 
				lfz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].z ; 

				rky = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].y ;
				lky = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].y ;
				rkx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].x ;
				lkx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].x ;		  
				rkz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].z ; 
				lkz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].z ; 

							cout << "Right Foot.y: " ;
							cout << rfy << endl ;
							cout << "Left Foot.y: " ;
							cout << lfy << endl ;
							cout << "Right Foot.x: " ;
							cout << rfx << endl ;
							cout << "Left Foot.x: " ;
							cout << lfx << endl ;
							cout << "Right Foot.z: " ;
							cout << rfz << endl ;
							cout << "Left Foot.z: " ;
							cout << lfz << endl ; 

							cout << "Right Knee.y: " ;
							cout << rky << endl ;
							cout << "Left Knee.y: " ;
							cout << lky << endl ;
							cout << "Right Knee.x: " ;
							cout << rkx << endl ;
							cout << "Left Knee.x: " ;
							cout << lkx << endl ;
							cout << "Right Knee.z: " ;
							cout << rkz << endl ;
							cout << "Left Knee.z: " ;
							cout << lkz << endl ; 

							ofs << "Right Foot.y: "  << rfy << endl ;
							ofs << "Left Foot.y: "  << lfy << endl ;
							ofs << "Right Foot.x: "  << rfx << endl ;
							ofs << "Left Foot.x: "  << lfx << endl ;
							ofs << "Right Foot.z: "  << rfz << endl ;
							ofs << "Left Foot.y: "  << lfz << endl ;
							ofs << endl ;

							ofs << "Right Knee.y: "  << rky << endl ;
							ofs << "Left Knee.y: "  << lky << endl ;
							ofs << "Right Knee.x: "  << rkx << endl ;
							ofs << "Left Knee.x: "  << lkx << endl ;
							ofs << "Right Knee.z: "  << rkz << endl ;
							ofs << "Left Knee.y: "  << lkz << endl ;
							ofs << endl ;
		
    }
    cvShowImage("Frame",frame);
    NuiImageStreamReleaseFrame( h2, pImageFrame );
    cvWaitKey(30);

	Sleep(100);

	}
}

else if ( choise == 7) 
	{

		ofs << " Recorded Knee and Foot " << endl << " Sample Frequency (Hz) " << endl << " 100 Hz / 0,1 sec " << endl << endl ;

while(1)
{

    WaitForSingleObject(SkeletonEvent, INFINITE);
    //get skeleton data
    hr = NuiSkeletonGetNextFrame(0, &skeletonFrame);
    if( hr != S_OK )
        continue;
    NuiTransformSmooth(&skeletonFrame, NULL);
    //get color image data
    const NUI_IMAGE_FRAME * pImageFrame = NULL;
    hr = NuiImageStreamGetNextFrame(h2, 0, &pImageFrame );
    if( hr != S_OK )
        continue;

    INuiFrameTexture *pTexture = pImageFrame->pFrameTexture;
    NUI_LOCKED_RECT LockedRect;
    pTexture->LockRect( 0, &LockedRect, NULL, 0 );
   
	//Draw skeleton points on frame 
    for(int i=0; i<6; i++){
        NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;
        if (NUI_SKELETON_TRACKED == trackingState) 
          
                cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT],640, 480,pImageFrame), 4, CV_RGB(255,0,0),2);
				cvCircle(frame,SkeletonToScreen(skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT],640, 480,pImageFrame), 4, CV_RGB(151,13,220),2);

				rfy = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].y ;
				lfy = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].y ;
				rfx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].x ;
				lfx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].x ;		  
				rfz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_RIGHT].z ; 
				lfz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_FOOT_LEFT].z ; 

				rky = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].y ;
				lky = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].y ;
				rkx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].x ;
				lkx = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].x ;		  
				rkz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_RIGHT].z ; 
				lkz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_KNEE_LEFT].z ; 

				ray = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].y ;
				lay = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].y ;
				rax = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].x ;
				lax = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].x ;		  
				raz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_RIGHT].z ; 
				laz = skeletonFrame.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_ANKLE_LEFT].z ;

							cout << "Right Foot.y: " ;
							cout << rfy << endl ;
							cout << "Left Foot.y: " ;
							cout << lfy << endl ;
							cout << "Right Foot.x: " ;
							cout << rfx << endl ;
							cout << "Left Foot.x: " ;
							cout << lfx << endl ;
							cout << "Right Foot.z: " ;
							cout << rfz << endl ;
							cout << "Left Foot.z: " ;
							cout << lfz << endl ; 

							cout << "Right Knee.y: " ;
							cout << rky << endl ;
							cout << "Left Knee.y: " ;
							cout << lky << endl ;
							cout << "Right Knee.x: " ;
							cout << rkx << endl ;
							cout << "Left Knee.x: " ;
							cout << lkx << endl ;
							cout << "Right Knee.z: " ;
							cout << rkz << endl ;
							cout << "Left Knee.z: " ;
							cout << lkz << endl ; 

							cout << "Right Ankle.y: " ;
							cout << ray << endl ;
							cout << "Left Ankle.y: " ;
							cout << lay << endl ;
							cout << "Right Ankle.x: " ;
							cout << rax << endl ;
							cout << "Left Ankle.x: " ;
							cout << lax << endl ;
							cout << "Right Ankle.z: " ;
							cout << raz << endl ;
							cout << "Left Ankle.z: " ;
							cout << laz << endl ; 

							ofs << "Right Foot.y: "  << rfy << endl ;
							ofs << "Left Foot.y: "  << lfy << endl ;
							ofs << "Right Foot.x: "  << rfx << endl ;
							ofs << "Left Foot.x: "  << lfx << endl ;
							ofs << "Right Foot.z: "  << rfz << endl ;
							ofs << "Left Foot.y: "  << lfz << endl ;
							ofs << endl ;

							ofs << "Right Knee.y: "  << rky << endl ;
							ofs << "Left Knee.y: "  << lky << endl ;
							ofs << "Right Knee.x: "  << rkx << endl ;
							ofs << "Left Knee.x: "  << lkx << endl ;
							ofs << "Right Knee.z: "  << rkz << endl ;
							ofs << "Left Knee.y: "  << lkz << endl ;
							ofs << endl ;

							ofs << "Right Ankle.y: "  << ray << endl ;
							ofs << "Left Ankle.y: "  << lay << endl ;
							ofs << "Right Ankle.x: "  << rax << endl ;
							ofs << "Left Ankle.x: "  << lax << endl ;
							ofs << "Right Ankle.z: "  << raz << endl ;
							ofs << "Left Ankle.y: "  << laz << endl ;
							ofs << endl ;
		
    }
    cvShowImage("Frame",frame);
    NuiImageStreamReleaseFrame( h2, pImageFrame );
    cvWaitKey(30);

	Sleep(100);

	}
}

else 
{
	cout << "Press a number between 1-7!" << endl;
	continue;
}

}

	NuiShutdown();  //shut down the Kinect
  
	ofs.close();

	return 0;
}

